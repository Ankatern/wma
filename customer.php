<?php
/**
 * @author Vasiliy Abolmasov <ankaten@gmail.com>
 **/

include 'dbworker.php';

class Customer extends DBWorker
{
    /**
     * Table name
     *
     * @var string
     */
    const TABLE_NAME = 'customer';

    /**
     * Columns for table
     *
     * @var array
     */
    const TABLE_COLUMNS = ['name'];

    /**
     * Customer name
     *
     * @var string
     */
    protected $name;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    /**
     * Load customer by id
     *
     * @param int $id
     * @return bool
     */
    public function load( int $id )
    {
        $answerFlag = false;
        $data = $this->getFromDb($id);
        if ($data) {
            $answerFlag = true;
            $this->name = $data['name'];
        }
        return $answerFlag;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string|null $name
     * @return bool
     */
    public function setName(string $name = null):bool
    {
        $answerFlag = false;
        if (!is_null($name)) {
            $answerFlag = true;
            $this->name = $name;
        }
        return $answerFlag;
    }

    /**
     * Save
     *
     * @return bool
     */
    public function save()
    {
        $dataToSave['name'] = $this->name;

        return $this->saveDB(
            self::TABLE_COLUMNS,
            $dataToSave
        );
    }
}