<?php
/**
* @author Vasiliy Abolmasov <ankaten@gmail.com>
**/
abstract class DBWorker
{
    /**
    * Connection
    *
    * @var PDO
    **/
    protected $db;

    /**
    * Table name
    *
    * @var string
    **/
    protected $tableName;

    /**
    * Id
    *
    * @var int
    **/
    protected $id;

    /**
     * createdAtTimestamp
     *
     * @var int
     **/
    protected $createdAtTimestamp;

    /**
     * createdAtFormatted
     *
     * @var string
     **/
    protected $createdAtFormatted;

    /**
     * DBWorker constructor.
     * @param string $tableName
     */
    function __construct( 
         string $tableName
    ) {
        $this->tableName = $tableName;
        
        try {
            // Подключение к бд в файле
            $this->db = new PDO('sqlite:/var/www/test/test'); 
        } catch( Exception $e ) {
            //We should log it, but i just will show error
            echo $e;
        }
    }

    /**
     * getter for id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Load element by id
     *
     * @param int|null $id
     * @return mixed
     */
    public function getFromDb(
        int $id = null 
    ) {
        if (isset($id))
        {
            try {
                //создаем запрос и получаем данные
                $stmt = $this->db->prepare('SELECT * from ' . $this->tableName . ' WHERE id =:id');
                $stmt->bindParam(':id', $id);
                $stmt->execute();
                
                //Возвращаем следующую строку в виде массива, индексированного именами столбцов
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                if (is_array($result)) {
                    $this->id = $result['id'];
                    $this->createdAtTimestamp = $result['created_at'];
                    $this->createdAtFormatted = $this->formatDateFromTimestamp($result['created_at']);
                }
                return $result;
            } catch( Exception $e ) {
                //We should log it, but i just will show error
                echo $e;
            }
        }
    }


    /**
     * Delete element
     *
     * @param int|null $id
     * @return bool
     */
    public function delete(int $id = null):bool
    {
        //preset $id
        if (is_null($id)) {
            if (!is_null($this->id)) {
                $id = $this->id;
            } else {
                return false;
            }
        }

        //delete row
        try {
            //create request and delete row
            $stmt = $this->db->prepare('DELETE FROM ' . $this->tableName . ' WHERE id=:id');
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            return true;
        } catch( Exception $e ) {
            //We should log it, but i just will show error
            echo $e;
            return false;
        }
    }

    public function saveDB(array $columns, array $data )
    {
        if (is_null($this->id)) {
            $sql = "INSERT INTO " . $this->tableName;

            //add columns to sql
            $columnsStr = implode(',', $columns);
            $sqlValues = ' VALUES(';
            foreach ($columns as $column) {
                $sqlValues .= ":$column,";
            }
            $sqlValues = substr($sqlValues, 0, -1);
            $sqlValues .= ')';
            $sql .="($columnsStr) $sqlValues";
        } else {
            $sql = 'UPDATE ' . $this->tableName . ' SET';
            $sqlValues = ' ';
            foreach ($columns as $column) {
                $sqlValues .= "$column = :$column,";
            }
            $sqlValues = substr($sqlValues, 0, -1);
            $sql .= $sqlValues . ' WHERE id = ' . $this->id;
        }

        $stmt = $this->db->prepare($sql);
        //add values to stmt
        foreach ($columns as $column) {
            $stmt->bindParam(":$column", $data['$column']);
        }
        //execute it
        return $stmt->execute();
    }

    /**
     * Format date from timestamp
     *
     * @param int $timestamp
     * @param string $format
     * @return false|string
     */
    protected function formatDateFromTimestamp(int $timestamp, string $format = 'd-m-Y H:i:s')
    {
        return date($format, $timestamp);
    }
}
