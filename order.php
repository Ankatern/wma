<?php
/**
 * @author Vasiliy Abolmasov <ankaten@gmail.com>
 **/
include 'dbworker.php';
include 'paramstrait.php';

class Order extends DBWorker
{
    use ParamsTrait;

    /**
     * Table name
     *
     * @var string
     */
    const TABLE_NAME = 'orders';

    /**
     * Columns for table
     *
     * @var array
     */
    const TABLE_COLUMNS = ['params'];

    /**
     * Params array
     *
     * @var array
     */
    protected $params = [];

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    /**
     * Load order by id
     *
     * @param int $id
     * @return bool
     */
    public function load( int $id )
    {
        $answerFlag = false;
        $data = $this->getFromDb($id);
        if ($data) {
            $answerFlag = true;
            if (!is_null($data['params'])) {
                $this->params = $this->fromJson($data['params']);
            }
        }
        return $answerFlag;
    }

    /**
     * Set param value by it`s path
     *
     * @param string $paramPath
     * @param $value
     * @return bool
     */
    public function setParam(
        string $paramPath,
        $value
    ) {
        return $this->setParamValue($this->params, $paramPath, $value);
    }

    /**
     * Get param by path
     *
     * @param string $paramPath
     * @return mixed
     */
    public function getParam(
        string $paramPath
    ) {
        return $this->getParamValue($this->params, $paramPath);
    }

    /**
     * Unset param by path
     *
     * @param string $paramPath
     * @return bool
     */
    public function unsetParam(
        string $paramPath
    ) {
        return $this->deleteParamValue($this->params, $paramPath);
    }

    /**
     * Unset params
     *
     * @param array|string $paramsPath
     * @return array|bool
     */
    public function unsetParams(
        $paramsPath
    ) {
        return $this->deleteParamsValue($this->params, $paramsPath);
    }

    /**
     * Save
     *
     * @return bool
     */
    public function save()
    {
        $dataToSave['params'] = $this->toJson($this->params);

        return $this->saveDB(
            self::TABLE_COLUMNS,
            $dataToSave
        );
    }

    /**
     * Remove Params
     */
    public function removeParams()
    {
        $this->removeParamsValues($this->params);
    }
}