<?php
/**
 * @author Vasiliy Abolmasov <ankaten@gmail.com>
 **/
include 'dbworker.php';
include 'paramstrait.php';

class Product extends DBWorker
{
    use ParamsTrait;

    /**
     * Table name
     *
     * @var string
     */
    const TABLE_NAME = 'product';

    /**
     * Columns for table
     *
     * @var array
     */
    const TABLE_COLUMNS = ['title', 'params'];

    /**
     * Customer name
     *
     * @var string
     */
    protected $title;

    /**
     * Params array
     *
     * @var array
     */
    protected $params = [];

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        parent::__construct(self::TABLE_NAME);
    }

    /**
     * Load product by id
     *
     * @param int $id
     * @return bool
     */
    public function load( int $id )
    {
        $answerFlag = false;
        $data = $this->getFromDb($id);
        if ($data) {
            $answerFlag = true;
            $this->title = $data['title'];
            if (!is_null($data['params'])) {
                $this->params = $this->fromJson($data['params']);
            }
        }
        return $answerFlag;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string|null $title
     * @return bool
     */
    public function setTitle(string $title = null):bool
    {
        $answerFlag = false;
        if (!is_null($title)) {
            $answerFlag = true;
            $this->title = $title;
        }
        return $answerFlag;
    }

    /**
     * Set param value by it`s path
     *
     * @param string $paramPath
     * @param $value
     */
    public function setParam(
        string $paramPath,
        $value
    ) {
        $this->setParamValue($this->params, $paramPath, $value);
        var_dump($this->params);
    }

    /**
     * Get param by path
     *
     * @param string $paramPath
     * @return mixed
     */
    public function getParam(
        string $paramPath
    ) {
        return $this->getParamValue($this->params, $paramPath);
    }

    /**
     * Unset param by path
     *
     * @param string $paramPath
     * @return bool
     */
    public function unsetParam(
        string $paramPath
    ) {
        return $this->deleteParamValue($this->params, $paramPath);
    }

    /**
     * Unset params
     *
     * @param array|string $paramsPath
     * @return array|bool
     */
    public function unsetParams(
        $paramsPath
    ) {
        return $this->deleteParamsValue($this->params, $paramsPath);
    }


    /**
     * Remove Params
     */
    public function removeParams()
    {
        $this->removeParamsValues($this->params);
    }

    /**
     * Save
     *
     * @return bool
     */
    public function save()
    {
        $dataToSave['title'] = $this->title;
        $dataToSave['params'] = $this->toJson($this->params);

        return $this->saveDB(
            self::TABLE_COLUMNS,
            $dataToSave
        );
    }
}