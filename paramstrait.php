<?php
/**
 * @author Vasiliy Abolmasov <ankaten@gmail.com>
 **/
trait ParamsTrait {
    private $paramPathDelimiter = '.';

    /**
     * From json
     *
     * @param string $json
     * @return mixed
     */
    public function fromJson( string $json ){
        return json_decode($json, true);
    }

    /**
     * To json
     *
     * @param array $array
     * @return false|string
     */
    public function toJson( array $array ){
        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Prepare path from string to array
     *
     * @param string $path
     * @return array
     */
    protected function preparePath( string $path ) {
        return explode($this->paramPathDelimiter, $path);
    }

    /**
     * Set value to param by path
     *
     * @param $param
     * @param $path
     * @param $value
     * @return bool
     */
    public function setParamValue(&$param, $path, $value) {
        $pathArr = $this->preparePath($path);

        return $this->addDeepParamValue($pathArr, $param, $value);
    }

    /**
     * Add value to param or go deeper
     *
     * @param $pathArr
     * @param $arr
     * @param $value
     * @return bool
     */
    protected function addDeepParamValue($pathArr, &$arr, $value){
        $key = array_shift($pathArr);
        $arr[$key] = [];
        //was it last key in list?
        if (empty($pathArr)) {
            //add value by key
            $arr[$key] = $value;
            return true;
        }
        //take current element and go deeper
        return $this->addDeepParamValue($pathArr, $arr[$key], $value);
    }

    /**
     * Prepare path and get value
     *
     * @param $param
     * @param $path
     * @return mixed
     */
    public function getParamValue($param, $path) {
        $pathArr = $this->preparePath($path);

        return $this->getDeepParamValue($pathArr, $param);
    }

    /**
     * Get value from param by path
     *
     * @param $pathArr
     * @param $arr
     * @return mixed
     */
    protected function getDeepParamValue($pathArr, &$arr){
        $key = array_shift($pathArr);

        //if there are no such element. return null
        if (!array_key_exists($key, $arr)) {
            return null;
        }

        //was it last key in list?
        if (empty($pathArr)) {
            return $arr[$key];
        }
        //take current element and go deeper
        return $this->getDeepParamValue($pathArr, $arr[$key]);
    }

    /**
     * Prepare path and delete value
     *
     * @param $param
     * @param $path
     * @return bool
     */
    public function deleteParamValue(&$param, $path) {
        $pathArr = $this->preparePath($path);

        return $this->deleteDeepParamValue($pathArr, $param);
    }

    /**
     * Delete param by path
     *
     * @param $pathArr
     * @param $arr
     * @return bool
     */
    protected function deleteDeepParamValue($pathArr, &$arr){
        $key = array_shift($pathArr);

        if (!array_key_exists($key, $arr)) {
            return false;
        }

        if (empty($pathArr)) {
            unset($arr[$key]);
            return true;
        }

        if ($this->getDeepParamValue($pathArr, $arr[$key])) {
            unset($arr[$key]);
            return true;
        }
    }

    /**
     * Prepare paths and delete one by one
     *
     * @param $param
     * @param $paramsPath
     * @return array|bool
     */
    public function deleteParamsValue(&$param, $paramsPath){
        if (!is_string($paramsPath) && !is_array($paramsPath)) {
            return false;
        }

        //flag that show, deleted value or not
        $deleteFlags = [];

        if (is_string($paramsPath)) {
            $paramsPath = explode(',', $paramsPath);
        }

        foreach ($paramsPath as $paramPath) {
            $deleteFlags[$paramPath] = $this->deleteParamValue($param, $paramPath);
        }
        return $deleteFlags;
    }

    /**
     * Remove params
     *
     * @param $params
     */
    public function removeParamsValues( &$params )
    {
        $params = [];
    }
}